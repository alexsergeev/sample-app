module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = 'CS 232 Ruby on Rails Development'
    if page_title.empty?
      return base_title
    else
      return page_title + ' | ' + base_title
    end
  end

end
